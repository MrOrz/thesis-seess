\chapter{Introduction}
\label{ch:introduction}


% 1st paragraph, general background & niche

Cascading Style Sheet (CSS) is a fundamental web language for
describing the presentation of web pages, such as colors,
fonts, and layout. It is primarily designed to enable the separation
of content from presentation, and has been a W3C standard since 1996~\cite{lie97css, W3C:CSS1}.

One of its key benefits is reusing the same CSS
files and rules on the same page and across multiple web pages, which reduces repetition,
increases download speed, provides a consistent look and feel,
and improves maintainability. CSS reuse is a common practice.
For example, on Apple and McDonald's US
websites, each CSS file is referenced by an average of 22.8 and 6.1
HTML files respectively.

\begin{figure}[!t]
\centering
\includegraphics[width=\columnwidth]{figures/scenario-new-01.eps}


\caption{
  Modifying a CSS rule may fix a bug on the current page, but introduce bugs elsewhere:
  a developer fixes the ``Login'' button alignment by adding a
  \texttt{margin-left}  property to the login page;
  however, such change breaks the ``Login'' button layout
  in the navigation bar on the home page. This bug is not immediately visible to developers
  using currently available tools.
}
\label{fig:scenario}
\end{figure}

When modifying a CSS rule, developers need to first visually check that the
intended change has been correctly implemented. Then they need to
verify that the change does not introduce unintended bugs on
all parts of the site that reference the modified CSS files and reuse the
modified CSS rules~\cite{Keller:WWW09:CSS}. Figure~\ref{fig:scenario}
shows an example where a simple change to fix an alignment bug of the Login
button on one page breaks the layout of other buttons elsewhere on the site.


% 3rd paragraph, previous research to problem “CSS hard to write & inspect change impact”

CSS is supposed to be a simple, declarative collection of style rules.
However, despite the fact that CSS is essentially not a programming language,
CSS authoring is often regarded as ``programming''
\cite{Keller:WWW09:CSS,Reed:2006:Converge}
and it is necessary to debug CSS code~\cite{Quint:DocEng07:EWS}.
Web developers have to deal with sophisticated cascading rules,
take care of selector specificity and watch out for selectors matching unwanted HTML elements.
Even the CSS 2.0 specification itself is too complex for people
to understand how CSS features interact~\cite{Badros:UIST99:CCSS}.
Last but not least,
visual properties of an element can come from rules scattered across multiple style sheets.
It is often not clear how a modification of CSS code will impact
the representation of a document~\cite{Quint:DocEng07:EWS}.



Many tools have been developed for web programming: inspector tools
such as Firebug~\cite{firebug} and Chrome and Safari's built-in
tools~\cite{devtools} help developers inspect rendered web pages individually, but do not
help track changes across a site.  Web UI testing tools such as Selenium~\cite{selenium},
Sahi~\cite{sahi}, and Sikuli~\cite{Yeh:UIST09:Sikuli} support UI automation
when performing functional testing of web pages, but may not detect
non-functional problems introduced through CSS changes.
Mogotest~\cite{mogotest} is a web-based regression testing tool that
visualizes changes on pages individually, but do not support interactive
development nor multi-page visualization.


% 4th paragraph: 用 change impact analysis for CSS is missing 帶到現況（沒這種東西），然後寫現在怎麼做。

% Change impact analysis determines the effect of source code change by a collection
% of techniques \cite{Ren:PLAN04:Chianti}.
% It enables the developer to fiddle around a piece of code, experimenting with
% different edits \cite{Ren:PLAN04:Chianti}.
% The time spent in debugging can be reduced, raising the developer
% productivity \cite{Ren:PLAN04:Chianti}.
% Change impact analysis tools are proposed, targeting different programming
% languages and software types \cite{Ren:PLAN04:Chianti, Goeritzer:ICSE11:IAIndustry, Maule:ICSE08:IAdb}.
% However, such tool for CSS authoring does not exist yet.
% Without the aid of change impact analysis, current developers can only rely on
% good coding styles, having a hard time maintaining a sophisticated and consistent
% set of style sheets for the entire website \cite{Quint:DocEng07:EWS}.
% Despite the considerable effort in code guidelines \cite{OOCSS, SMACSS} and
% CSS quality improvements
% \cite{Keller:QUATIC10:CSSQuality,Badros:UIST99:CCSS,Mesbah:ICSE12:AutoAnalysis},
% rather less attention has been paid to delivering CSS change impacts for the
% developers as a real-time feedback during development.


% 5th paragraph, our solution


\begin{figure}[!h]
\centering
\includegraphics[width=\columnwidth]{figures/implementation-02.eps}
\caption{
  \seess graphical user interface: the left panel corresponds to the code editor,
  and the right panel shows a list of visual changes associated with the
  most recent CSS modifications.
}
\label{fig:implementation}
\end{figure}


We present \seess, a system that tracks CSS change impact across a website and
visually presents parts of the site that have changed due to the CSS code modification.
As shown in Figure~\ref{fig:implementation}, \seess computes and shows all
visible changes before and after the modification. %,
%and sorts them based on how much has changed in each of the cropped regions.
It also renders and stores snapshots of all pages across the website,
and compares them across revisions.

%When the developer saves a CSS or HTML document, snapshots of the web pages are
%taken.
%The snapshots of each webpage are versioned and stored separately.
% Whenever a CSS modification is made,
% \seess finds out which pages includes the current CSS document,
% and compares their current snapshots with the previous ones.
% After that, it shows within the code editor the screenshots of the changed pages,
% resized and cropped to the changed portion of the page.
% The screenshots are sorted so that supposedly the ``surprising'' changes are
% obvious to the developer.
% \seess provides a comprehensive overview of CSS change impact over the whole website.
% The ultimate goal of \seess is to help developers find problems right after they
% break anything.


%The design of \seess prototype is developed in 2 iterations.
% We adopt iterative design methodology to develop \seess prototype.
% In this research, we run the development of \seess in 2 iterations.
% \seess is developed in 2 design iterations. The first iteration
% We develope \seess prototype in an iterative fashion

We developed 2 \seess prototypes, one after another,
under an iterative design~\cite{Nielsen:Comp93:IterUIDesign} process.
In the 1st iteration, we develope a \seess prototype that tracks
HTML and CSS references of a static site when documents are opened and saved.
A small lab-based user study shows promising initial results.
All of the 4 participants reported that they were faster at fixing CSS problems with \seess
and all would like to continue using \seess as their regular web development tool.
The 2nd iteration starts with a brainstorming session with 8 web developers.
The 2nd prototype improves the performance and applicability across different types
of websites, aiming for public release.
The evaluation of this iteration is currently ongoing.


We reported our initial results of the 1st iteration in a research paper,
`SeeSS: Seeing What I Broke -- Visualizing Change Impact of Cascading Style Sheets (CSS)',
which was accepted for presentation at and publication in the proceedings of the
26th annual ACM symposium on User interface software and technology~\cite{Liang:UIST13:SeeSS}.


% 6th paragraph: overview of paper.

This thesis reports the design process of \seess so far.
We first compare related web developer tools and previous researches in web testing in Chapter~\ref{ch:relatedwork}.
We then describe the design and implementation of \seess in Chapter~\ref{ch:1stimplementation}.
Afterwards, we present our preliminary user study process in Chapter~\ref{ch:1stevaluation}.
The first iteration ends as we discuss the user study results in Chapter~\ref{ch:1stdiscussion}.

In the second iteration we first explore posibilities of presentations of change impacts via
a brain-storming session with 8 web developers in Chapter~\ref{ch:brainstorm}.
Based on that, we then describe the design choices in Chapter~\ref{ch:2ndimplementation}
and what we choose to leave out in Chapter \ref{ch:futurework}.
Lastly, we conclude this thesis with a summary of the 2 iterations in Chapter~\ref{ch:conclusion}.

%The essential CSS knowledge is introduced in the background section, followed by
%a list of common CSS design challenges front-end developers and developers face.
%After that, we describe how our system is structured and our choice of implementation.
%Later, we conduct an informal user study for qualitative feedback.
%Finally, we conclude and provide possible room for improvement in the conclusion
%and future work section.
